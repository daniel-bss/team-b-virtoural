'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SpotTrip extends Model {
    static associate(models) {
      SpotTrip.belongsTo(models.Destinations, {foreignKey: "destination_id"})
      SpotTrip.belongsTo(models.Streamers, {foreignKey: "streamer_id"})
    }
  }
  SpotTrip.init({
    name: DataTypes.STRING,
    duration: DataTypes.INTEGER,
    price: DataTypes.FLOAT,
    image: DataTypes.STRING,
    description: DataTypes.TEXT,
    address: DataTypes.TEXT,
    operating_hour: DataTypes.STRING,
    destination_id: DataTypes.INTEGER,
    streamer_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'SpotTrip',
  });
  return SpotTrip;
};