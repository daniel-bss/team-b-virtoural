'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class OptionalPackages extends Model {
    static associate(models) {
      OptionalPackages.belongsTo(models.Streamers, {foreignKey: "streamer_id"})
    }
  }
  OptionalPackages.init({
    streamer_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    price: DataTypes.FLOAT,
    image: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'OptionalPackages',
  });
  return OptionalPackages;
};