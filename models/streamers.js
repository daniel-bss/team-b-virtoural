'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Streamers extends Model {
    static associate(models) {
      Streamers.belongsToMany(models.Destinations, {through: models.StreamersDestinations, foreignKey: "streamer_id"})
      Streamers.hasMany(models.SpotTrip, {foreignKey: "streamer_id"})
      Streamers.hasMany(models.OptionalPackages, {foreignKey: "streamer_id"})
      Streamers.hasOne(models.Checkouts, {foreignKey: "streamer_id"})
    }
  }
  Streamers.init({
    uuid: { 
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    streamer_fullname: DataTypes.STRING,
    streamer_email: DataTypes.STRING,
    description: DataTypes.TEXT,
    streamer_password: DataTypes.STRING,
    is_streamer: DataTypes.BOOLEAN,
    birthday: DataTypes.STRING,
    streamer_address: DataTypes.STRING,
    phone_number: DataTypes.STRING,
    gender: DataTypes.STRING,
    image_streamer: DataTypes.STRING,
    rating: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Streamers',
  });
  return Streamers;
};