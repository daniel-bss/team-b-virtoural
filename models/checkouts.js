'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Checkouts extends Model {
    static associate(models) {
      Checkouts.belongsTo(models.Users, {foreignKey: "user_id"})
      Checkouts.belongsTo(models.Destinations, {foreignKey: "destination_id"})
      Checkouts.belongsTo(models.Streamers, {foreignKey: "streamer_id"})
    }
  }
  Checkouts.init({
    user_id: DataTypes.INTEGER,
    destination_id: DataTypes.INTEGER,
    streamer_id: DataTypes.INTEGER,
    date: DataTypes.STRING,
    time: DataTypes.STRING,
    total_price: DataTypes.INTEGER,
    address: DataTypes.TEXT,
    payment_image: DataTypes.TEXT,
    zoom_link: DataTypes.TEXT,
    selected_trips: DataTypes.STRING,
    selected_packages: DataTypes.STRING,
    is_confirmed: DataTypes.BOOLEAN,
    is_approved: DataTypes.BOOLEAN,
    is_paid: DataTypes.BOOLEAN,
    is_finished: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Checkouts',
  });
  return Checkouts;
};