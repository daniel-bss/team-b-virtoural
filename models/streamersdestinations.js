'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StreamersDestinations extends Model {
    static associate(models) {
      
    }
  }
  StreamersDestinations.init({
    destination_id: DataTypes.INTEGER,
    streamer_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'StreamersDestinations',
  });
  return StreamersDestinations;
};