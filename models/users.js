'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    static associate(models) {
      Users.hasMany(models.Comments, {foreignKey: "user_id"})
      Users.hasMany(models.Checkouts, {foreignKey: "user_id"})
      // Users.belongsToMany(models.Destinations, {through: models.Checkouts})
    }
  }
  Users.init({
    uuid: { 
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    user_fullname: DataTypes.STRING,
    user_email: DataTypes.STRING,
    user_password: DataTypes.STRING,
    is_streamer: DataTypes.BOOLEAN,
    role: DataTypes.STRING,
    profile_picture: DataTypes.STRING,
    occupation: DataTypes.STRING,
    city: DataTypes.STRING,
    user_address: DataTypes.TEXT,
    phone_number: DataTypes.STRING,
    gender: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Users',
  });
  return Users;
};