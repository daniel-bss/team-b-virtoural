'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Destinations extends Model {
    static associate(models) {
      Destinations.belongsToMany(models.Streamers, {through: models.StreamersDestinations, foreignKey: 'destination_id'})
      Destinations.hasMany(models.SpotTrip, {foreignKey: "destination_id"})
      Destinations.hasMany(models.Checkouts, {foreignKey: "destination_id"})
    }
  }
  Destinations.init({
    destination_name: DataTypes.STRING,
    rating: DataTypes.STRING,
    total_streamer: DataTypes.INTEGER,
    total_spot: DataTypes.INTEGER,
    image: DataTypes.STRING,
    description: DataTypes.TEXT,
    price: DataTypes.INTEGER,
    date: DataTypes.STRING,
    time: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Destinations',
  });
  return Destinations;
};