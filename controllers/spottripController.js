const { SpotTrip } = require("../models")

class SpotTripController{
  static async getAllSpotTrip(req, res) {
    try {
      const data = await SpotTrip.findAll()
      res.status(200).json({ data })

    } catch (error) {
      console.log(error)
    }
  } 

  static async getOneSpotTrip(req, res) {
    const id = req.params.id
    const options = {
      where: {
        id
      }
    }

    const data = await SpotTrip.findOne(options)
    
    res.status(200).json({ data })
  } 

  static async createSpotTrip(req, res) {
    const { name, duration, price, image, description, address, operating_hour, destination_id, streamer_id } = req.body

    const newSpotTrip = await SpotTrip.create({
      name, 
      duration, 
      price, 
      image, 
      description, 
      address, 
      operating_hour, 
      destination_id, 
      streamer_id
    })

    res.status(200).json({ data: newSpotTrip })
  } 

  static async updateSpotTrip(req, res) {
    const { name, duration, price, image, destination_id, streamer_id } = req.body

    const payload = {
      name, duration, price, image, destination_id, streamer_id
    }

    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }

    const updatedSpotTrip = await SpotTrip.update(payload, options)

    res.status(200).json({ data: updatedSpotTrip })
  } 

  static async deleteSpotTrip(req, res) {
    const id = req.params.id
    const deletedSpotTrip = await SpotTrip.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({ data: deletedSpotTrip })
  } 
}

module.exports = SpotTripController;
