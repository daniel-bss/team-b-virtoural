const { OptionalPackages } = require("../models")

class OptionalPackageController{
  static async getAllOptionalPackage(req, res) {
    try {
      const data = await OptionalPackages.findAll()
      res.status(200).json({data})

    } catch (error) {
      console.log(error)
    }
  } 

  static async getOneOptionalPackage(req, res) {
    const id = req.params.id
    const options = {
      where: {
        id
      },
    }
    const data = await OptionalPackages.findOne(options)

    res.status(200).json({ data })
  } 

  static async createOptionalPackage(req, res) {
    const { name, price, image, streamer_id } = req.body
    const payload = {
      name, price, image, streamer_id
    }

    const newOptionalPackages = await OptionalPackages.create(payload)

    res.status(200).json({ data: newOptionalPackages })
  } 

  static async updateOptionalPackage(req, res) {
    const { name, price, image, streamer_id } = req.body

    const payload = {
      name, price, image, streamer_id
    }

    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }

    const updatedOptionalPackages = await OptionalPackages.update(payload, options)

    res.status(200).json({ data: updatedOptionalPackages })
  } 

  static async deleteOptionalPackage(req, res) {
    const id = req.params.id
    const deletedOptionalPackages = await OptionalPackages.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({ data: deletedOptionalPackages })
  } 

}

module.exports = OptionalPackageController;
