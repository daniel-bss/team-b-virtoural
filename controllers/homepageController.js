const bcrypt = require('bcrypt')
const passport = require('passport')
const models = require('../models')

class HomepageController {

    static async renderHomepage(req, res) {
        const streamers = await models.Streamers.findAll()
        // const destinations = await models.Destinations.findAll({offset: 3, limit: 12})
        // const destinations = await models.Destinations.findAll()

        const destination_result = await models.Destinations.findAll({
        include: {
            model: models.SpotTrip,
        }
        })
        try {
            console.log(req.user.id)
        } catch (err) {
            console.log(err)
        }

        res.render("homepage.ejs", {
            streamers,
            destinations: destination_result,
            user: req.user
        })
    }

    static async register(req, res) {
        const {
            name,
            email,
            psw,
            opttobestr,
            bday,
            address,
            nohp,
            gender
        } = req.body
        
        const fields_check = () => {
            if (opttobestr == "tidak") {
                const fields = ["name", "email", "psw", "opttobestr"]
                return fields
            } else if (opttobestr == "iya") {
                const fields = ["name", "email", "psw", "opttobestr", "bday", "address", "nohp", "gender"]
                return fields
            }
        }
    
        // Errors check
        let have_errors = 0
        
        // Check password length
        if (psw.length < 6) {
            req.flash('password_length', 'Password minimal harus 6 karakter!')
            have_errors++
        }
    
        // Check input from user
        const fields = fields_check()
        for (let i = 0; i < fields.length; i++) {
            if (req.body[fields[i]].length === 0) {
                req.flash("input_check", "Pastikan semua data terisi!")
                have_errors++
            }
            break
        }
    
        const email_check = async () => {
            if (opttobestr == "tidak") {
                const email_result = await models.Users.findOne({ where: { user_email: email } })
                return email_result
            } else if (opttobestr == "iya") {
                const email_result = await models.Streamers.findOne({ where: { streamer_email: email } })
                return email_result
            }
        }
    
        email_check().then(result => {
            if (result) {
                req.flash("email_exists", "Email anda sudah terdaftar!")
                res.redirect('/?register=true')
            } else if (have_errors) {
                res.redirect('/?register=true')
            } else {
                bcrypt.hash(psw, 10).then(async (hash, reject) => {
                    if (opttobestr == "tidak") {
                        console.log(hash)
                        await models.Users.create({
                            user_fullname: name,
                            user_email: email,
                            user_password: hash,
                            is_streamer: false,
                            role: "user",
                            profile_picture: "",
                            occupation: "",
                            city: "",
                            user_address: "",
                            phone_number: "",
                            gender: ""
                        });
                    } else if (opttobestr == "iya") {
                        await models.Streamers.create({
                            streamer_fullname: name,
                            streamer_email: email,
                            streamer_password: hash,
                            description: "",
                            is_streamer: true,
                            birthday: bday,
                            streamer_address: address,
                            phone_number: nohp,
                            gender,
                            image_streamer: "",
                            rating: 0
                        })
                    }
                })
                req.flash("register_success", "Pendaftaran sukses, silahkan login!")
                res.redirect('/?login=true')
            }
        })
    }

    static async login(req, res, next) {
        passport.authenticate('local', {
            successRedirect: '/',
            failureRedirect: '/?login=true',
            failureFlash: true
        })(req, res, next)
    }

    static async logout(req, res) {
        req.logout()
        req.flash('logout_success', 'Log out berhasil')
        res.redirect('/')
    }
}


module.exports = HomepageController
