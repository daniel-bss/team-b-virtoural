const { Comments } = require("../models");

class CommentController{
  static async getAllComment(req, res){
    try {
      const data = await Comments.findAll()
      res.status(200).json({ data })

    } catch (error) {
      console.log(error)
    }
  }

  static async getOneComment(req, res){
    const id = req.params.id
    const options = {
      where: {
        id
      }
    }

    const data = await Comments.findOne(options)
    
    res.status(200).json({ data })
  }

  static async createComment(req, res){
    const { user_id, rating, comment } = req.body
    const payload = {
      user_id, rating, comment
    }

    const newComment = await Comments.create(payload)

    res.status(200).json({ data: newComment })
  }

  static async updateComment(req, res){
    const { user_id, rating, comment } = req.body

    const payload = {
      user_id, rating, comment
    }

    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }

    const updatedComment = await Comments.update(payload, options)

    res.status(200).json({ data: updatedComment })
  }

  static async deleteComment(req, res){
    const id = req.params.id
    const deletedComment = await Comments.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({ data: deletedComment })
  }
}

module.exports = CommentController;