const models = require("../models");
const UUID = require("uuid");
const { encrypt } = require("../utils")

class StreamerController{
  static async getAllStreamer(req, res){
    try {
      const data = await models.Streamers.findAll()
      res.status(200).json({data})
    } catch (error) {
      console.log(error)
    }
  }

  static async getOneStreamer(req, res){
    /**
     * Function untuk READ 1 Streamer, beserta SpotTrips-nya dan OptionalPackages-nya yang dia tawarkan kepada user
     */
    const id = req.params.id
    const streamer_result = await models.Streamers.findOne({
      where: {
        id
      },
      include: [
        {
          model: models.SpotTrip
        },
        {
          model: models.OptionalPackages
        }
      ]
    })
    // const streamer_result = await models.Streamers.findOne({
    //   where: {
    //     id
    //   },
    //   include: [
    //     {
    //       model: models.SpotTrip,
    //       where: {
    //         id: [1, 2]
    //       }
    //     },
    //     {
    //       model: models.OptionalPackages
    //     }
    //   ]
    // })

    
    // res.json({
    //   streamer_result
    // })
    res.render("streamer-page.ejs", {
      streamer_result,
      user: req.user
    })
  }

  static async createStreamer(req, res){
    const uuid = UUID.v4()
    let { streamer_fullname, streamer_email, streamer_password, is_streamer, birthday, streamer_address, phone_number, gender } = req.body
    streamer_password = encrypt.encryptPwd(streamer_password)
    const payload = {
      uuid, streamer_fullname, streamer_email, streamer_password, is_streamer, birthday, streamer_address, phone_number, gender
    }

    const newStreamer = await models.Streamers.create(payload)
    res.status(200).json({data: newStreamer})
  }

  static async updateStreamer(req, res){
    const { streamer_fullname, streamer_email, streamer_password, is_streamer, birthday, streamer_address, phone_number, gender } = req.body

    const payload = {
      streamer_fullname, streamer_email, streamer_password, is_streamer, birthday, streamer_address, phone_number, gender
    }

    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }

    const updatedStreamer = await models.Streamers.update(payload, options)

    res.status(200).json({data: updatedStreamer})
  }

  static async deleteStreamer(req, res){
    const id = req.params.id
    const deletedStreamer = await models.Streamers.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({ data: deletedStreamer })
  }
}

module.exports = StreamerController;