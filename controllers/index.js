const CommentController = require("./commentsController");
const CheckoutController = require("./checkoutsController");
const DestinationController = require("./destinationsController");
const OptionalPackageController = require("./optionalpackagesController");
const SpotTripController = require("./spottripController");
const StreamerController = require("./streamersController");
const StreamerDestinationController = require("./streamersDestinationsController");
const UserController = require("./usersController");
const HomepageController = require("./homepageController");
const InsertData = require('../utils/insert-data')

module.exports = {
  CommentController,
  CheckoutController,
  DestinationController,
  OptionalPackageController,
  SpotTripController,
  StreamerController,
  StreamerDestinationController,
  UserController,
  HomepageController,
  InsertData
}