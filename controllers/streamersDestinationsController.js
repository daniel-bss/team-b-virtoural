const { StreamersDestinations } = require("../models");

class StreamerDestinationController{
  static async getAllStreamerDestination(req, res){
    try {
      const data = await StreamersDestinations.findAll()
      res.status(200).json({ data })
    } catch (error) {
      console.log(error)
    }
  }

  static async getOneStreamerDestination(req, res){
    const id = req.params.id
    const options = {
      where: {
        id
      }
    }
    const data = await StreamersDestinations.findOne(options)

    res.status(200).json({ data })
  }

  static async createStreamerDestination(req, res){
    try {
      const { destination_id, streamer_id } = req.body
      const payload = {
        destination_id, streamer_id
      }

      const newStreamerDestination = await StreamersDestinations.create(payload)
      res.status(200).json({ data: newStreamerDestination })
    } catch (error) {
      console.log(error)
    }
  }

  static async updateStreamerDestination(req, res){
    const { destination_id, streamer_id } = req.body

    const payload = {
      destination_id, streamer_id
    }
    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }
    const updatedStreamerDestination = await StreamersDestinations.update(payload, options)

    res.status(200).json({
      data: updatedStreamerDestination
    })
  }

  static async deleteStreamerDestination(req, res){
    const id = req.params.id
    const deletedStreamerDestination = await StreamersDestinations.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({
      data: deletedStreamerDestination
    })
  }
}

module.exports = StreamerDestinationController;