const { Users } = require("../models");
const models = require("../models")
const UUID = require("uuid");
const { encrypt } = require("../utils")

class UserController{
  static async getAllUser(req, res){
    try {
      const data = await Users.findAll()
      res.status(200).json({data})
    } catch (error) {
      console.log(error)
    }
  }

  static async getOneUser(req, res){
    const id = req.params.id
    const options = {
      where: {
        id
      },
    }
    const data = await Users.findOne(options)

    res.status(200).json({ data })
  }

  static async renderUserProfile(req, res) {
    const user_result = await models.Users.findOne({
      where: {
        uuid: req.params.id
      }
    })

    console.log(user_result)
    
    res.render("user-profile.ejs", {
      user_result,
      user: req.user
    })
  }

  static async updateUserProfile(req, res) {
    const uuid = req.params.id
    const user_result = await models.Users.findOne({
      where: {
        uuid
      }
    })
    
    const payload = {}
    // for (let key in req.body) {
    //   console.log(key)
    //   console.log(req.body.key)
    // }
    const {user_fullname, gender, user_email, phone_number, user_address, city} = req.body

    user_result.update({user_fullname, gender, user_email, phone_number, user_address, city})

    res.redirect(`/user/profile/${uuid}`)
  }

  static async createUser(req, res){
    const uuid = UUID.v4()
    let { user_fullname, user_email, user_password, is_streamer, role, profile_picture, occupation } = req.body
    user_password = encrypt.encryptPwd(user_password)
    const payload = {
      uuid, user_fullname, user_email, user_password, is_streamer, role, profile_picture, occupation
    }

    const newUser = await Users.create(payload)
    res.status(200).json({data: newUser})
  }

  static async updateUser(req, res){
    const { user_fullname, user_email, user_password, is_streamer, role, profile_picture, occupation } = req.body

    const payload = {
      user_fullname, user_email, user_password, is_streamer, role, profile_picture, occupation
    }

    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }

    const updatedUser = await Users.update(payload, options)

    res.status(200).json({data: updatedUser})
  }

  static async deleteUser(req, res){
    const id = req.params.id
    const deletedUser = await Users.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({ data: deletedUser })
  }
}

module.exports = UserController;