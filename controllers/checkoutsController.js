const { type } = require("express/lib/response");
const models = require("../models");

class CheckoutController{
  static async getAllCheckout(req, res){
    try {
      const data = await models.Checkouts.findAll()
      res.status(200).json({ data })

    } catch (error) {
      console.log(error)
    }
  }

  static async getOneCheckout(req, res){
    const id = req.params.id
    const options = {
      where: {
        id
      }
    }

    const data = await models.Checkouts.findOne(options)
    
    res.status(200).json({ data })
  }

  static async createCheckout(req, res){
    let { date, time, spottrip, paket } = req.body
    
    if (typeof(spottrip) == "object") {
      for (let i = 0; i < spottrip.length; i++) {
        spottrip[i] = Number(spottrip[i])
      }
    } else {
      spottrip = Number(spottrip)
    }

    if (typeof(paket) == "object") {
      for (let i = 0; i < paket.length; i++) {
        paket[i] = Number(paket[i])
      }
    } else {
      paket = Number(paket)
    }
    

    const spottrip_result = await models.SpotTrip.findAll({
      where: {
        id: spottrip // tipe data array dari req.body.spottrip
      },
      include: [
        {
          model: models.Destinations
        },
        {
          model: models.Streamers
        }
      ]
    })
    
    const optionalpackage_result = await models.OptionalPackages.findAll({
      where: {
        id: paket // tipe data array dari req.body.paket_opsional
      },
      include: {
        model: models.Streamers
      }
    })
    
    let total_price = 0
    
    // Kalkulasi total harga dari SpotTrip dan OptionalPackages yang dipilih user
    for (let trip of spottrip_result) {
      total_price += trip.dataValues.price
    }
    for (let paket of optionalpackage_result) {
      total_price += paket.dataValues.price
    }

    const user_result = await models.Users.findOne({
      where: {
        uuid: req.user.uuid
      }
    })

    const newCheckout = await models.Checkouts.create({
      user_id: user_result.id,
      destination_id: spottrip_result[0].dataValues.destination_id,
      streamer_id: spottrip_result[0].dataValues.streamer_id,
      date,
      time,
      total_price,
      address: "",
      payment_image: "",
      zoom_link: "",
      selected_trips: JSON.stringify(spottrip),
      selected_packages: JSON.stringify(paket),
      is_confirmed: null,
      is_approved: null,
      is_paid: null,
      is_finished: null
    })

    // res.status(200).json({ data: newCheckout })
    // res.json({
    //   spottrip_result,
    //   optionalpackage_result
    // })

    res.redirect(`/checkout/render/${newCheckout.id}`)
  }

  static async renderOneCheckout(req, res) {
    const checkout_id = req.params.id
    
    const checkout_result = await models.Checkouts.findOne({
      where: {
        id: checkout_id
      }, 
      include: {
        model: models.Streamers
      }
    })

    const spottrip_result = await models.SpotTrip.findAll({
      where: {
        id: JSON.parse(checkout_result.selected_trips)
      },
      include: {
        model: models.Destinations
      }
    })

    const optionalpackage_result = await models.OptionalPackages.findAll({
      where: {
        id: JSON.parse(checkout_result.selected_packages)
      }
    })

    // console.log("\ncheckout_result:", checkout_result)
    // console.log("\nspottrip_result:", spottrip_result)
    // console.log("\noptionalpackage_result:", optionalpackage_result)
    

    res.render("order-page.ejs", {
      checkout_result,
      spottrip_result,
      optionalpackage_result,
      user: req.user
    })
    // res.json({
    //   checkout_result,
    //   spottrip_result,
    //   optionalpackage_result,
    //   user: req.user
    // })
  }

  static async renderMyTicket(req, res) {
    // const checkout_result = await models.Checkouts.findAll({
    try {
      const checkout_result = await models.Checkouts.findOne({
        where: {
          user_id: req.params.id
        }, 
        include: [
          {
            model: models.Streamers
          },
          {
            model: models.Destinations
          }
        ]
      })

      const spottrip_result = await models.SpotTrip.findAll({
        where: {
          id: JSON.parse(checkout_result.selected_trips)
        },
        include: {
          model: models.Destinations
        }
      })

      let destinasi = []
      for (let spot of spottrip_result) {
        destinasi.push(spot.name)
      }

      destinasi = destinasi.join(", ")

      res.render("user-ticket-page.ejs", {
        checkout_result: checkout_result,
        destinasi,
        user: req.user
      })
      
    } catch (err) {
      res.redirect("/")
    }


    
    // res.json({
    //   checkout_result: checkout_result[checkout_result.length - 1],
    //   destinasi,
    //   user: req.user
    // })
  }

  static async updateCheckout(req, res){
    const { user_id, destination_id, total_price, is_paid } = req.body

    const payload = {
      user_id, destination_id, total_price, is_paid
    }

    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }

    const updatedCheckout = await models.Checkouts.update(payload, options)

    res.status(200).json({ data: updatedCheckout })
  }

  static async updateUserConfirmation(req, res) {
    const checkout_result = await models.Checkouts.findOne({
      where: {
        id: req.params.id
      }
    })
    checkout_result.update({
      is_confirmed: true
    })
    res.redirect(`/checkout/render/${req.params.id}`)
  }

  static async abortUserConfirmation(req, res) {
    const checkout_result = await models.Checkouts.findOne({
      where: {
        id: req.params.id
      }
    })
    console.log("AEHBAHSBAS")
    checkout_result.update({
      is_confirmed: null
    })
    res.redirect(`/checkout/render/${req.params.id}`)
  }

  static async abortOrder(req, res){
    const checkout_result = await models.Checkouts.findOne({
      where: {
        id: req.params.id
      }
    })

    checkout_result.destroy()
    res.redirect('/')
  }

  static async deleteCheckout(req, res){
    const id = req.params.id
    const deletedCheckout = await models.Checkouts.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({ data: deletedCheckout })
  }
}

module.exports = CheckoutController;