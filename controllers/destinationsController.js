const models = require("../models");

class DestinationController{

  static async getAllDestination(req, res){
    try {
      const data = await models.Destinations.findAll()
      res.status(200).json({ data })

    } catch (error) {
      console.log(error)
    }
  }

  static async getOneDestination(req, res) {
    /**
     * Function untuk READ 1 Destination
     */
    const destinasi = req.params.destinasi

    const destination_result = await models.Destinations.findOne({
      where: {
        id: destinasi
      },
      include: [
        {
          model: models.SpotTrip,
        }
      ]
    })

    const streamer_result = await models.Streamers.findAll({
      include: [
        {
          model: models.Destinations,
          where: {
            id: destinasi
          }
        },
        {
          model: models.SpotTrip,
          where: {
            destination_id: destinasi
          }
        }
      ]
    })

    // res.json({
    //   destination_result,
    //   streamer_result
    // })
    res.render("destinasi-page.ejs", {
      destination_result, 
      streamer_result, 
      user: req.user
    })
  }

  static async getDestinationWithSpotTrip(req, res) {
    /**
     * Function untuk READ 1 Destination dan 1 SpotTrip yang dipilih
     * Ketika user hit '/destination/:destinasi/:spottrip', maka pada halaman homepage akan dikirim data Destinasi dan SpotTrip yang dipilih, beserta Streamers yang tersedia
     */
    const destinasi = req.params.destinasi
    const spottrip_id = req.params.spottrip
    
    // Untuk menampilkan semua SpotTrip
    const all_spottrip = await models.Destinations.findOne({
      where: {
        id: destinasi
      },
      include: {
        model: models.SpotTrip
      }
    })

    // Untuk menampilkan data 1 SpotTrip saja
    const spottrip = await models.SpotTrip.findOne({
      where: {
        id: spottrip_id
      }
    })


    const destination_result = await models.Destinations.findOne({
      where: {
        id: destinasi
      },
      include: {
        model: models.SpotTrip,
        where: {
          id: spottrip.id
        }
      }
    })

    res.render("destinasi-page.ejs", {
      destination_result, 
      all_spottrip, 
      user: req.user,
      spottrip_id
    })
    // res.json({
    //   destination_result, 
    //   all_spottrip, 
    //   user: req.user
    // })
  }

  static async createDestination(req, res){
    const { destination_name, rating, total_streamer, total_spot, image, description } = req.body
    const payload = {
      destination_name, rating, total_streamer, total_spot, image, description
    }

    const newDestination = await models.Destinations.create(payload)

    res.status(200).json({ data: newDestination })
  }

  static async updateDestination(req, res){
    const { destination_name, rating, total_streamer, total_spot, image, description } = req.body

    const payload = {
      destination_name, rating, total_streamer, total_spot, image, description
    }

    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }

    const updatedDestination = await models.Destinations.update(payload, options)

    res.status(200).json({ data: updatedDestination })
  }

  static async deleteDestination(req, res){
    const id = req.params.id
    const deletedDestination = await models.Destinations.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({ data: deletedDestination })
  }
}

module.exports = DestinationController;