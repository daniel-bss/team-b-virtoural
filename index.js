const express = require('express')
const flash = require('connect-flash')
const session = require('express-session')
const passport = require('passport')
require('./config/passport')(passport)

const app = express()
const PORT = process.env.PORT || 3000

// Using EJS and setting 'public' folder as the root
app.set('view engine', 'ejs')
app.use(express.static('public'));

// Session and Passport Initialization
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}))
app.use(passport.initialize())
app.use(passport.session())

// Connect Flash to send error messages
app.use(flash())

// Create Global Variables with 'res.locals'
app.use((req, res, next) => {
  // Register Error Message
  res.locals.password_length = req.flash('password_length')
  res.locals.input_check = req.flash('input_check')
  // res.locals.password_confirmation = req.flash('password_confirmation')
  res.locals.email_exists = req.flash('email_exists')
  res.locals.register_success  =req.flash('register_success')
  
  // Login Error message
  res.locals.login_error = req.flash('login_error')
  res.locals.error = req.flash('error')

  // Logout Success
  res.locals.logout_success = req.flash('logout_success')

  next() // continues to next middleware
})

// Body Parser
app.use(express.json()) // for JSON request
app.use(express.urlencoded({extended: false})) // for Form request

// Route
app.use(require('./routes/routes'));

app.listen(PORT, () => console.log(`\nServer is running on port http://localhost:${PORT}\n\n`))
