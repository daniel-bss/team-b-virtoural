const authentication = require("./authentication")
const userAuthorization = require("./userAuthorization")

module.exports = {
  authentication,
  userAuthorization
}