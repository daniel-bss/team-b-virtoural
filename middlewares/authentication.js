const { token } = require("../utils")

function authentication(req, res, next){
  const access_token = req.headers.token
  const payload = access_token ? token.decodeToken(access_token) : 0
  console.log(payload, "<<<<")

  if(!payload || !access_token){
    res.status(401).json({
      status: 401,
      msg: "Silahkan Login"
    })
  } else {
    req.userLogin = payload
    console.log(req.userLogin, "++++")
    next()
  }
}

module.exports = authentication