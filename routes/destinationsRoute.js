const { DestinationController } = require("../controllers");
const router = require("express").Router();

router.get("/", DestinationController.getAllDestination) // EDITED
router.get("/spottrips/:destinasi", DestinationController.getOneDestination) // EDITED
router.post("/", DestinationController.createDestination)
router.put("/:id", DestinationController.updateDestination)
router.delete("/:id", DestinationController.deleteDestination)

module.exports = router;