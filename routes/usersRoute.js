const { UserController } = require("../controllers");
const router = require("express").Router();

router.get("/", UserController.getAllUser)
router.get("/:id", UserController.getOneUser)
router.get("/profile/:id", UserController.renderUserProfile)
router.post("/profile/:id", UserController.updateUserProfile)
router.post("/", UserController.createUser)
router.put("/:id", UserController.updateUser)
router.delete("/:id", UserController.deleteUser)

module.exports = router;