const express = require("express");
const router = express.Router();
const { ensureAuthenticated } = require('../config/auth')

const checkoutRoute = require("./checkoutsRoute")
const commentRoute = require("./commentsRoute")
const destinationRoute = require("./destinationsRoute")
const optionalPackageRoute = require("./optionalPackagesRoute")
const spotTripRoute = require("./spottripRoute")
const streamerDestinationRoute = require("./streamersDestinantionsRoute")
const streamerRoute = require("./streamersRoute")
const userRoute = require("./usersRoute")
const homepageRoute = require('./homepageRoute')

const {InsertData} = require('../controllers');

router.use("/checkout", checkoutRoute)
router.use("/comment", commentRoute)
router.use("/destination", destinationRoute)
router.use("/optionalpackage", optionalPackageRoute)
router.use("/spottrip", spotTripRoute)
router.use("/streamerdestination", streamerDestinationRoute)
router.use("/streamer", ensureAuthenticated, streamerRoute)
router.use("/user", userRoute)

// HOMEPAGE
router.use(homepageRoute);

// router.get('/mencoba' ,(req, res) => {
//     console.log(req.body)
//     // res.render("user-profile.ejs", {user: req.user})
//     res.render("coretan.ejs")
// })

// router.post('/mencoba' ,(req, res) => {
//     console.log(req.body)
//     // res.render("user-profile.ejs", {user: req.user})
//     res.send("OK")
// })
// INSERT DATA TO HEROKU DATABASE
router.post('/insert-data', InsertData)

module.exports = router;
