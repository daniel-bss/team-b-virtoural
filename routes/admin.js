const { Router } = require('express')
const router = Router()

const { Users, SpotTrip, Destinations, Streamers, OptionalPackages } = require('../models')

router.get('/', (req, res) => {
    res.json({msg: "Test..."})
})

router.get('/destinations', async (req, res) => {
    res.json({msg: "SELURUH DESTINASI"})
})

router.post('/destinations', async (req, res) => {
    const { 
        destination_name, 
        rating, 
        total_streamer,
        total_spot,
        image, 
        description
    } = req.body

    const destinations = await Destinations.create({
        destination_name, 
        rating, 
        total_streamer, 
        image, 
        description
    })

    res.json(destinations)
})

router.get('/destinations/:destination', async (req, res) => {
    const destination = req.params.destination
    res.json({ destination })
})

router.get('/spot-trip', async (req, res) => {
    const spot_trips = await SpotTrip.findAll()
    res.json(spot_trips)
})

router.post('/spot-trip', async (req, res) => {
    const { name, duration, price, image, streamer, destination } = req.body
    const destination_result = await Destinations.findOne({
        where: {destination_name: destination}
    })
    const streamer_result = await Streamers.findOne({
        where: {streamer_fullname: streamer}
    })
    const spot_trips = await SpotTrip.create({ name, 
        duration, 
        price, 
        image, 
        destination_id: destination_result.id, 
        streamer_id: streamer_result.id 
    })
    res.json(spot_trips)
})

router.get('/streamers', async (req, res) => {
    const streamers = await Streamers.findAll({
        include: [
            {
                model: SpotTrip,
                attributes: {
                    exclude: ["id", "createdAt", "updatedAt"]
                }
            },
            {
                model: OptionalPackages
            }
        ],
        attributes: {
            exclude: ["uuid", "createdAt", "updatedAt"]
        }
    })
    res.json(streamers)
})

router.post('/streamers', async (req, res) => {
    const {
        streamer_fullname,
        streamer_email,
        streamer_password,
        is_streamer,
        birthday,
        streamer_address,
        phone_number,
        gender
    } = req.body
    const streamers = await Streamers.create({
        streamer_fullname,
        streamer_email,
        streamer_password,
        is_streamer,
        birthday,
        streamer_address,
        phone_number,
        gender
    })
    res.json(streamers)
})

router.get('/optional-packages', async (req, res) => {
    const optional_packages = await OptionalPackages.findAll({
        attributes: {
            exclude: ["createdAt", "updatedAt"]
        }
    })
    res.json(optional_packages)
})

router.post('/optional-packages', async (req, res) => {
    const {
        name,
        price,
        image,
        streamer
    } = req.body
    const streamer_result = await Streamers.findOne({where: {streamer_fullname: streamer}})
    const optional_packages = await OptionalPackages.create({
        name,
        price,
        image,
        streamer_id: streamer_result.id
    })
    res.json(optional_packages)
})

router.post('/checkout', async (req, res) => {
    // const {
    //     user,
    //     streamer,
    //     spottrip,
    //     optional_packages
    // } = req.body
    // spottrip = ["asia-afrika", "jalan-braga"]
    // optionalpackages = ["Pisang Bollen - Cokelat"]
    // const user_result = await Users.findOne({where: {user_fullname: user}})
    // const streamer_result = await Streamers.findOne({
    //     where: {streamer_fullname: streamer},
    //     include: [
    //         {
    //             model: SpotTrip
    //         },
    //         {
    //             model: OptionalPackages
    //         }
    //     ]
    // })
    
    // const bandung = Destinations.findOne(where: bandung, {include:  model: SpotTrip}).spottrips
    // const OptionalPackages = OptionalPackages.findOne(where: bandung, {include:  model: SpotTrip}).spottrips
    // for (const x in spottrip) {
    //     for (const i in badnung) {
    //         if i = x:
    //             totalprice += x.price
    //     }
    // }
    
    // for (const x in optionalpackages) {
    //     for (const i in OptionalPackages) {
    //         if i = x:
    //             totalprice += x.price
    //     }
    // }

    // Checkouts.create({
    //     total_price,
    //     is_paid: false
    // })

    // console.log(streamer_result.SpotTrips)
    // res.json(streamer_result)


})


module.exports = router
