const { CheckoutController } = require("../controllers");
const router = require("express").Router();

router.get("/", CheckoutController.getAllCheckout)
router.get("/:id", CheckoutController.getOneCheckout)
router.get("/render/:id", CheckoutController.renderOneCheckout)
router.get('/myticket/:id', CheckoutController.renderMyTicket)
router.post("/", CheckoutController.createCheckout) // EDITED
router.put("/:id", CheckoutController.updateCheckout)
router.post("/user_confirmation/:id", CheckoutController.updateUserConfirmation)
router.post("/user_confirmation/cancel/:id", CheckoutController.abortUserConfirmation)
router.post("/user_confirmation/cancelorder/:id", CheckoutController.abortOrder)
router.delete("/:id", CheckoutController.deleteCheckout)

module.exports = router;