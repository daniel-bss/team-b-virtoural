const { SpotTripController } = require("../controllers");
const router = require("express").Router();

router.get("/", SpotTripController.getAllSpotTrip)
router.get("/:id", SpotTripController.getOneSpotTrip)
router.post("/", SpotTripController.createSpotTrip)
router.put("/:id", SpotTripController.updateSpotTrip)
router.delete("/:id", SpotTripController.deleteSpotTrip)

module.exports = router;