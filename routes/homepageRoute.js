const { HomepageController } = require("../controllers");
const router = require("express").Router();


router.get("/", HomepageController.renderHomepage)
router.post("/register", HomepageController.register)
router.post("/login", HomepageController.login)
router.get('/logout', HomepageController.logout)

module.exports = router;
