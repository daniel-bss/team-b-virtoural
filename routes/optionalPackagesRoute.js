const { OptionalPackageController } = require("../controllers");
const router = require("express").Router();

router.get("/", OptionalPackageController.getAllOptionalPackage)
router.get("/:id", OptionalPackageController.getOneOptionalPackage)
router.post("/", OptionalPackageController.createOptionalPackage)
router.put("/:id", OptionalPackageController.updateOptionalPackage)
router.delete("/:id", OptionalPackageController.deleteOptionalPackage)

module.exports = router;