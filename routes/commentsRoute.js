const { CommentController } = require("../controllers");
const router = require("express").Router();

router.get("/", CommentController.getAllComment)
router.get("/:id", CommentController.getOneComment)
router.post("/", CommentController.createComment)
router.put("/:id", CommentController.updateComment)
router.delete("/:id", CommentController.deleteComment)

module.exports = router;