const { StreamerController } = require("../controllers");
const router = require("express").Router();

router.get("/", StreamerController.getAllStreamer)
router.get("/:id", StreamerController.getOneStreamer)
router.post("/", StreamerController.createStreamer)
router.put("/:id", StreamerController.updateStreamer)
router.delete("/:id", StreamerController.deleteStreamer)

module.exports = router;