const { StreamerDestinationController } = require("../controllers");
const router = require("express").Router();

router.get("/", StreamerDestinationController.getAllStreamerDestination)
router.get("/:id", StreamerDestinationController.getOneStreamerDestination)
router.post("/", StreamerDestinationController.createStreamerDestination)
router.put("/:id", StreamerDestinationController.updateStreamerDestination)
router.delete("/:id", StreamerDestinationController.deleteStreamerDestination)

module.exports = router;