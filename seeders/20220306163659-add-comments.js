'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Comments', [
      {
        user_id: 1,
        rating: 4,
        comment: 'Temukan destinasi yang Anda inginkan dan lakukan tour virtualmu tanpa harus keluar rumah',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id: 2,
        rating: 5,
        comment: 'Virtoural Keren! Awalnya skeptis, tapi ternyata menarik banget!',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id: 3,
        rating: 4,
        comment: 'Liburan dari kasur seru juga ternyata, solusi nih buat kalian yang kerja lembur bagai kuda kaya saya hahaha',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id: 4,
        rating: 5,
        comment: 'Temukan destinasi yang Anda inginkan dan lakukan tour virtualmu tanpa harus keluar rumah',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id: 5,
        rating: 3,
        comment: 'Virtoural Keren! Awalnya skeptis, tapi ternyata menarik banget!',
        createdAt: new Date(),
        updatedAt: new Date()
      }
  
    ], {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Comments', null, {});
  }
};
