'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Checkouts', [
      {
        user_id: 1,
        destination_id: 1,
        streamer_id: 1,
        date: '16 Maret 2022',
        time: '12.00',
        total_price: 100000,
        address: 'Jl. Kampung Rambutan',
        payment_image: '',
        zoom_link: 'https://us02web.zoom.us/j/88521219919',
        selected_trips: '',
        selected_packages: '',
        is_confirmed: false,
        is_approved: false,
        is_paid: false,
        is_finished: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id: 2,
        destination_id: 2,
        streamer_id: 2,
        date: '16 Maret 2022',
        time: '11.00',
        total_price: 150000,
        address: 'Jl. Jendral Sudirman',
        payment_image: '',
        zoom_link: 'https://us02web.zoom.us/j/88521219919',
        selected_trips: '',
        selected_packages: '',
        is_confirmed: false,
        is_approved: false,
        is_paid: false,
        is_finished: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id: 3,
        destination_id: 3,
        streamer_id: 3,
        date: '16 Maret 2022',
        time: '10.00',
        total_price: 900000,
        address: 'Jl. Kota Wisata',
        payment_image: '',
        zoom_link: 'https://us02web.zoom.us/j/88521219919',
        selected_trips: '',
        selected_packages: '',
        is_confirmed: false,
        is_approved: false,
        is_paid: false,
        is_finished: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id: 4,
        destination_id: 4,
        streamer_id: 4,
        date: '16 Maret 2022',
        time: '11.00',
        total_price: 500000,
        address: 'Jl. Kota Baru',
        payment_image: '',
        zoom_link: 'https://us02web.zoom.us/j/88521219919',
        selected_trips: '',
        selected_packages: '',
        is_confirmed: false,
        is_approved: false,
        is_paid: false,
        is_finished: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id: 5,
        destination_id: 5,
        streamer_id: 5,
        date: '16 Maret 2022',
        time: '13.00',
        total_price: 1800000,
        address: 'Jl. Ahmad Yani',
        payment_image: '',
        zoom_link: 'https://us02web.zoom.us/j/88521219919',
        selected_trips: '',
        selected_packages: '',
        is_confirmed: false,
        is_approved: false,
        is_paid: false,
        is_finished: false,
        createdAt: new Date(),
        updatedAt: new Date()
      }
  
    ], {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Checkouts', null, {});
  }
};
