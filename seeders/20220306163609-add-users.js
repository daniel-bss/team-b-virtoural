'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Users', [
      {
        uuid: '7aa605fd-5a96-4445-a807-393cb5fafb99',
        user_fullname: 'Barack Obama',
        user_email: 'barack@gmail.com',
        user_password: '123Desember',
        is_streamer: false,
        role: 'user',
        profile_picture: 'barackObama.png',
        city: 'Bogor',
        user_address: 'Jl. Batu Besar',
        phone_number: '089689897654',
        occupation: 'Politikus',
        gender: 'pria',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: '6b6f43e8-742b-4222-8016-0d22ff3f7014',
        user_fullname: 'Fahmi Gustado',
        user_email: 'fahmi@gmail.com',
        user_password: '123Desember',
        is_streamer: false,
        role: 'user',
        profile_picture: 'fahmi-gustado.png',
        city: 'Jakarta',
        user_address: 'Jl. Pasar Senin',
        phone_number: '0856898784',
        occupation: 'Mahasiswa',
        gender: 'pria',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: 'd311a003-b907-4684-a74c-9b9f2ea92035',
        user_fullname: 'Angel Brilliant',
        user_email: 'angel@gmail.com',
        user_password: '123Desember',
        is_streamer: false,
        role: 'admin',
        profile_picture: 'angel-img.png',
        city: 'Jakarta',
        user_address: 'Jl. Narogong',
        phone_number: '081345678889',
        occupation: 'Dokter',
        gender: 'wanita',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: '98fc54f3-bab2-4f07-bc0a-cd0db8bd57a2',
        user_fullname: 'Maharani',
        user_email: 'maharani@gmail.com',
        user_password: '123Desember',
        is_streamer: false,
        role: 'admin',
        profile_picture: 'maharani.png',
        city: 'Bekasi',
        user_address: 'Jl. Narogong',
        phone_number: '081345678889',
        occupation: 'CEO',
        gender: 'wanita',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: 'f0d25414-f529-4387-8396-a4cc8b5a3613',
        user_fullname: 'Pamungkas',
        user_email: 'pamungkas@gmail.com',
        user_password: '123Desember',
        is_streamer: false,
        role: 'admin',
        profile_picture: 'pamungkas.png',
        city: 'Bogor',
        user_address: 'Jl. Narogong',
        phone_number: '081345678889',
        occupation: 'Programmer',
        gender: 'pria',
        createdAt: new Date(),
        updatedAt: new Date()
      }
  
    ], {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Users', null, {});
  }
};
