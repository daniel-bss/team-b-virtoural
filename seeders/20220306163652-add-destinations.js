'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Destinations', [
      {
        destination_name: 'Bandung',
        rating: '5',
        total_streamer: 20,
        total_spot: 10,
        image: 'bandung.png',
        description: 'Bandung memiliki 10 spot wisata terbaik yang bisa kamu jelajahi secara virtual. Penasaran bagaimana pengalaman menarik yang bisa kamu dapatkan? Yuk coba sekarang juga!',
        price: 17000,
        date: '6 Maret 2022',
        time: '9.00',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_name: 'Bali',
        rating: '4',
        total_streamer: 11,
        total_spot: 10,
        image: 'bali.png',
        description: 'Bali memiliki 10 spot wisata terbaik yang bisa kamu jelajahi secara virtual. Penasaran bagaimana pengalaman menarik yang bisa kamu dapatkan? Yuk coba sekarang juga!',
        price: 75000,
        date: '12 Maret 2022',
        time: '15.00',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_name: 'Raja Ampat',
        rating: '5',
        total_streamer: 7,
        total_spot: 8,
        image: 'papua.png',
        description: 'Papua memiliki 10 spot wisata terbaik yang bisa kamu jelajahi secara virtual. Penasaran bagaimana pengalaman menarik yang bisa kamu dapatkan? Yuk coba sekarang juga!',
        price: 150000,
        date: '22 Maret 2022',
        time: '10.00',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_name: 'Malang',
        rating: '3',
        total_streamer: 15,
        total_spot: 9,
        image: 'malang.png',
        description: 'Malang memiliki 10 spot wisata terbaik yang bisa kamu jelajahi secara virtual. Penasaran bagaimana pengalaman menarik yang bisa kamu dapatkan? Yuk coba sekarang juga!',
        price: 80000,
        date: '13 Maret 2022',
        time: '16.00',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_name: 'Bangka Belitung',
        rating: '4',
        total_streamer: 17,
        total_spot: 12,
        image: 'bangka.png',
        description: 'Bangka Belitung memiliki 10 spot wisata terbaik yang bisa kamu jelajahi secara virtual. Penasaran bagaimana pengalaman menarik yang bisa kamu dapatkan? Yuk coba sekarang juga!',
        price: 90000,
        date: '30 Maret 2022',
        time: '10.00',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_name: 'Medan',
        rating: '3',
        total_streamer: 13,
        total_spot: 10,
        image: 'medan.png',
        description: 'Medan memiliki 10 spot wisata terbaik yang bisa kamu jelajahi secara virtual. Penasaran bagaimana pengalaman menarik yang bisa kamu dapatkan? Yuk coba sekarang juga!',
        price: 13000,
        date: '13 Maret 2022',
        time: '11.00',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_name: 'Yogyakarta',
        rating: '3',
        total_streamer: 15,
        total_spot: 11,
        image: 'yogya.png',
        description: 'Yogyakarta memiliki 10 spot wisata terbaik yang bisa kamu jelajahi secara virtual. Penasaran bagaimana pengalaman menarik yang bisa kamu dapatkan? Yuk coba sekarang juga!',
        price: 100000,
        date: '13 Maret 2022',
        time: '12.00',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_name: 'Lombok',
        rating: '5',
        total_streamer: 10,
        total_spot: 5,
        image: 'bdg-bg.png',
        description: 'Bandung adalah tempat yang sangat menyenangkan',
        createdAt: new Date(),
        updatedAt: new Date()
      },
  
    ], {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Destinations', null, {});
  }
};
