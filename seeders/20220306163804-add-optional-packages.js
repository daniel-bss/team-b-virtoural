'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('OptionalPackages', [
      {
        name: "Kopi Aroma",
        price: 55000,
        image: "kopi-aroma.png",
        streamer_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "Cetak Foto Polaroid",
        price: 9000,
        image: "cetak-foto.png",
        streamer_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "Kopi Aroma",
        price: 55000,
        image: "kopi-aroma.png",
        streamer_id: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "Cetak Foto Polaroid",
        price: 9000,
        image: "cetak-foto.png",
        streamer_id: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "Pisang Bollen - Cokelat",
        price: 25000,
        image: "kopi-aroma.png",
        streamer_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "Pisang Bollen - Cokelat",
        price: 25000,
        image: "kopi-aroma.png",
        streamer_id: 3,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('OptionalPackages', null, {});
  }
};
