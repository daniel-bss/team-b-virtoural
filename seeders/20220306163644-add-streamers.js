'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Streamers', [
      {
        uuid: 'cdd668ef-7bcb-48ce-b540-09315ea56070',
        streamer_fullname: "Andrew",
        streamer_email: "andrew@email.com",
        description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.',
        streamer_password: "rahasia_tapi_belum_dihash",
        is_streamer: true,
        birthday: "15 Juli 1997",
        streamer_address: "Jakarta Selatan",
        phone_number: "08123123123",
        gender: "male",
        image_streamer: 'andrew.png',
        rating: 3,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: 'a44adf52-738c-41fd-b48d-d9c599530b45',
        streamer_fullname: 'Kamila',
        streamer_email: 'kamila@gmail.com',
        description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.',
        streamer_password: '123Desember',
        is_streamer: true,
        birthday: '25 Januari 1999',
        streamer_address: 'Jalan Mangka Ujung kanan no 8',
        phone_number: '08908989090',
        gender: 'female',
        image_streamer: 'kamila.png',
        rating: 5,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: 'ecaa4dc9-489a-4c39-be62-2ae5df6594ed',
        streamer_fullname: 'Mutia',
        streamer_email: 'mutia@gmail.com',
        description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.',
        streamer_password: '123Desember',
        is_streamer: true,
        birthday: '13 Juli 1996',
        streamer_address: 'Jalan Duren Ujung kiri no 8',
        phone_number: '0908032787',
        gender: 'female',
        image_streamer: 'mutia.png',
        rating: 4,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: '26a93d2c-1f1e-4704-a6ae-02806c0e707e',
        streamer_fullname: 'Putra',
        streamer_email: 'putra@gmail.com',
        description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.',
        streamer_password: '123Desember',
        is_streamer: true,
        birthday: '8 April 1997',
        streamer_address: 'Jalan Apel Ujung kanan no 8',
        phone_number: '085167789090',
        gender: 'male',
        image_streamer: 'putra.png',
        rating: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: '10f7a90b-b350-43a8-8d29-4d28062560bb',
        streamer_fullname: 'Adela',
        streamer_email: 'adela@gmail.com',
        description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.',
        streamer_password: '123Desember',
        is_streamer: true,
        birthday: '8 April 1997',
        streamer_address: 'Jalan Apel Ujung kanan no 8',
        phone_number: '085167789090',
        gender: 'female',
        image_streamer: 'adela.png',
        rating: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: '5fc475ad-5c1f-4a61-9a6c-5cd8c002c54f',
        streamer_fullname: 'Mirza',
        streamer_email: 'mirza@gmail.com',
        description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.',
        streamer_password: '123Desember',
        is_streamer: true,
        birthday: '8 April 1997',
        streamer_address: 'Jalan Apel Ujung kanan no 8',
        phone_number: '085167789090',
        gender: 'male',
        image_streamer: 'mirza.png',
        rating: 5,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: '9c6f78b5-87e3-4261-b2eb-94c4bdd6f8dd',
        streamer_fullname: 'Dimas',
        streamer_email: 'dimas@gmail.com',
        description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.',
        streamer_password: '123Desember',
        is_streamer: true,
        birthday: '8 April 1997',
        streamer_address: 'Jalan Apel Ujung kanan no 8',
        phone_number: '085167789090',
        gender: 'male',
        image_streamer: 'dimas.png',
        rating: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: '447f103f-2f39-4fe4-b2a9-1c033a6991da',
        streamer_fullname: 'Putri',
        streamer_email: 'putri@gmail.com',
        description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.',
        streamer_password: '123Desember',
        is_streamer: true,
        birthday: '8 April 1997',
        streamer_address: 'Jalan Apel Ujung kanan no 8',
        phone_number: '085167789090',
        gender: 'female',
        image_streamer: 'putri.png',
        rating: 5,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: 'abbb33e4-6966-4c09-8d19-e6a3b9994a55',
        streamer_fullname: 'Khalid',
        streamer_email: 'khalid@gmail.com',
        description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.',
        streamer_password: '123Desember',
        is_streamer: true,
        birthday: '8 April 1997',
        streamer_address: 'Jalan Apel Ujung kanan no 8',
        phone_number: '085167789090',
        gender: 'male',
        image_streamer: 'putra.png',
        rating: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: '10f7a90b-b350-43a8-8d29-4d28062560bb',
        streamer_fullname: 'Adela',
        streamer_email: 'adela@gmail.com',
        description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.',
        streamer_password: '123Desember',
        is_streamer: true,
        birthday: '8 April 1997',
        streamer_address: 'Jalan Apel Ujung kanan no 8',
        phone_number: '085167789090',
        gender: 'female',
        image_streamer: 'adela.png',
        rating: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: '5fc475ad-5c1f-4a61-9a6c-5cd8c002c54f',
        streamer_fullname: 'Mirza',
        streamer_email: 'mirza@gmail.com',
        description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.',
        streamer_password: '123Desember',
        is_streamer: true,
        birthday: '8 April 1997',
        streamer_address: 'Jalan Apel Ujung kanan no 8',
        phone_number: '085167789090',
        gender: 'male',
        image_streamer: 'mirza.png',
        rating: 5,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: '9c6f78b5-87e3-4261-b2eb-94c4bdd6f8dd',
        streamer_fullname: 'Dimas',
        streamer_email: 'dimas@gmail.com',
        description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.',
        streamer_password: '123Desember',
        is_streamer: true,
        birthday: '8 April 1997',
        streamer_address: 'Jalan Apel Ujung kanan no 8',
        phone_number: '085167789090',
        gender: 'male',
        image_streamer: 'dimas.png',
        rating: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: '447f103f-2f39-4fe4-b2a9-1c033a6991da',
        streamer_fullname: 'Putri',
        streamer_email: 'putri@gmail.com',
        description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.',
        streamer_password: '123Desember',
        is_streamer: true,
        birthday: '8 April 1997',
        streamer_address: 'Jalan Apel Ujung kanan no 8',
        phone_number: '085167789090',
        gender: 'female',
        image_streamer: 'putri.png',
        rating: 5,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: 'abbb33e4-6966-4c09-8d19-e6a3b9994a55',
        streamer_fullname: 'Khalid',
        streamer_email: 'khalid@gmail.com',
        description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.',
        streamer_password: '123Desember',
        is_streamer: true,
        birthday: '8 April 1997',
        streamer_address: 'Jalan Apel Ujung kanan no 8',
        phone_number: '085167789090',
        gender: 'male',
        image_streamer: 'khalid.png',
        rating: 5,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: '5022ca58-1009-4daa-bd3b-a94719b12db7',
        streamer_fullname: 'Thomas',
        streamer_email: 'thomas@gmail.com',
        description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.',
        streamer_password: '123Desember',
        is_streamer: true,
        birthday: '8 April 1997',
        streamer_address: 'Jalan Apel Ujung kanan no 8',
        phone_number: '085167789090',
        gender: 'male',
        image_streamer: 'thomas.png',
        rating: 5,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: '5e2fc5f8-18a4-4776-bb35-0f76821ace80',
        streamer_fullname: 'Sarah',
        streamer_email: 'sarah@gmail.com',
        description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.',
        streamer_password: '123Desember',
        is_streamer: true,
        birthday: '8 April 1997',
        streamer_address: 'Jalan Apel Ujung kanan no 8',
        phone_number: '085167789090',
        gender: 'female',
        image_streamer: 'sarah.png',
        rating: 5,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: 'bddc3403-e138-4f1c-a579-f72f5bd3ba3e',
        streamer_fullname: 'Mega',
        streamer_email: 'mega@gmail.com',
        description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.',
        streamer_password: '123Desember',
        is_streamer: true,
        birthday: '8 April 1997',
        streamer_address: 'Jalan Apel Ujung kanan no 8',
        phone_number: '085167789090',
        gender: 'female',
        image_streamer: 'mega.png',
        rating: 5,
        createdAt: new Date(),
        updatedAt: new Date()
      }

  
    ], {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Streamers', null, {});
  }
};
