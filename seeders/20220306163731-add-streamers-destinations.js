'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('StreamersDestinations', [
      {
        destination_id: 1,
        streamer_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_id: 1,
        streamer_id: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_id: 1,
        streamer_id: 3,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_id: 1,
        streamer_id: 4,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_id: 1,
        streamer_id: 5,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_id: 1,
        streamer_id: 6,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_id: 1,
        streamer_id: 7,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_id: 1,
        streamer_id: 8,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_id: 1,
        streamer_id: 9,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_id: 1,
        streamer_id: 10,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_id: 1,
        streamer_id: 11,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        destination_id: 1,
        streamer_id: 12,
        createdAt: new Date(),
        updatedAt: new Date()
      }
  
    ], {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('StreamersDestinations', null, {});
  }
};
