# Team B - virtoural

1. Buat .env file (pastikan variabelnya sesuai dengan config/config.js)
2. Buat Database, Jalankan sequelize db:create
3. Migrate Tabel, Jalankan sequelize db:migrate
4. Generate Seeder, Jalankan sequelize db:seed:all
5. Jalankan Project, nodemon index