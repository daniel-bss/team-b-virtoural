'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Checkouts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id'
      },
      onUpdate: 'cascade',
      onDelete: 'cascade'
      },
      destination_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Destinations',
          key: 'id'
      },
      onUpdate: 'cascade',
      onDelete: 'cascade'
      },
      streamer_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Streamers',
          key: 'id'
      },
      onUpdate: 'cascade',
      onDelete: 'cascade'
      },
      date: {
        type: Sequelize.STRING
      },
      time: {
        type: Sequelize.STRING
      },
      total_price: {
        type: Sequelize.INTEGER
      },
      address: {
        type: Sequelize.TEXT
      },
      payment_image: {
        type: Sequelize.TEXT
      },
      zoom_link: {
        type: Sequelize.TEXT
      },
      selected_trips: {
        type: Sequelize.STRING
      },
      selected_packages: {
        type: Sequelize.STRING
      },
      is_confirmed: {
        type: Sequelize.BOOLEAN
      },
      is_approved: {
        type: Sequelize.BOOLEAN
      },
      is_paid: {
        type: Sequelize.BOOLEAN
      },
      is_finished: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Checkouts');
  }
};