'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Streamers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      uuid: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      streamer_fullname: {
        type: Sequelize.STRING
      },
      streamer_email: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.TEXT
      },
      streamer_password: {
        type: Sequelize.STRING
      },
      is_streamer: {
        type: Sequelize.BOOLEAN
      },
      birthday: {
        type: Sequelize.STRING
      },
      streamer_address: {
        type: Sequelize.STRING
      },
      phone_number: {
        type: Sequelize.STRING
      },
      gender: {
        type: Sequelize.STRING
      },
      image_streamer: {
        type: Sequelize.STRING
      },
      rating: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Streamers');
  }
};