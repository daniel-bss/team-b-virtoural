module.exports = {
    ensureAuthenticated: (req, res, next) => {
        if (req.isAuthenticated()) {
            return next()
        }
        req.flash('login_error', 'Log in terlebih dahulu!')
        res.redirect('/?login=true')
    }
}