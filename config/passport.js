const {Strategy} = require('passport-local')
const bcrypt = require('bcrypt')
const {Users, Streamers} = require('../models') // users table

module.exports = (passport) => {
    passport.use(
        new Strategy({
            usernameField: "login_email",
            passwordField: "login_psw"
        }, 
        async (email, password, done) => {
            const user_result = await Users.findOne({where: { user_email: email }}) // returns null if doesn't exist!
            const streamer_result = await Streamers.findOne({where: { streamer_email: email }}) // returns null if doesn't exist!
            
            const check_users = () => {
                if (user_result && !streamer_result) {
                    return user_result
                } else if (streamer_result && !user_result) {
                    return streamer_result
                } else {
                    return null
                }
            }

            const result = check_users()
            if (!result) {
                return done(null, false, {message: 'Email tidak terdaftar!'}) // will be stored in 'error'
            }

            if (result.dataValues.user_password) {
                bcrypt.compare(password, result.dataValues.user_password, (err, isValid) => {
                    if (err) {console.log(err)}
                    if (isValid) {
                        done(null, result.dataValues) // will be sent to serializeUser()
                    } else {
                        return done(null, false, {message: "Password salah!"}) // will be stored in 'error'
                    }
                })
            } else {
                bcrypt.compare(password, result.dataValues.streamer_password, (err, isValid) => {
                    if (err) {console.log(err)}
                    if (isValid) {
                        done(null, result.dataValues) // will be sent to serializeUser()
                    } else {
                        return done(null, false, {message: "Password salah!"}) // will be stored in 'error'
                    }
                })
            }


        })
    )

    passport.serializeUser((user, done) => {
        done(null, user.uuid) // will be sent to deserializeUser() and stored in req.session.passport.user
    })

    passport.deserializeUser(async (uuid, done) => {
        const user_id = await Users.findOne({where: { uuid }})
        const streamer_id = await Streamers.findOne({where: { uuid }})

        if (user_id && !streamer_id) {
            done(null, user_id) // will be stored in req.user
        } else if (streamer_id && !user_id) {
            done(null, streamer_id) // will be stored in req.user
        } else {
            done(null, null) // will be stored in req.user
        }
    })
}
