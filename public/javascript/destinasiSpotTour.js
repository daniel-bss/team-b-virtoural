// const spotTripButton = document.querySelectorAll(".spot-trip-button");
// const spotTripContent = document.querySelectorAll(".spot-trip-card");

const spotTripButton = document.querySelectorAll(".spot-trip-button");
const spotTripCard = document.querySelectorAll(".spot-trip-card");

let activeButton = 0;

spotTripButton.forEach((item, i) => {
    item.addEventListener("click", () => {
        spotTripButton[activeButton].classList.remove("active");
        item.classList.add("active");
        activeButton = i;

        let tripBtnClasses = Array.from(item.classList)
        let spotTrip = tripBtnClasses[0]
        spotTripCard.forEach((card) => {
            let cardClasses = Array.from(card.classList)
            let cardTrip = cardClasses[0]
            if (spotTrip != cardTrip) {
                card.classList.add("hide")
            } else {
                card.classList.remove("hide")
            }
        })
    });
});


// spotTripButton.forEach((tripBtn) => {
//     let tripBtnClasses = Array.from(tripBtn.classList)
//     if (tripBtnClasses.includes("active")) {
//         let spotTrip = tripBtnClasses[tripBtnClasses.length - 1]
//         spotTripCard.forEach((card) => {
//             let cardClasses = Array.from(card.classList)
//             let cardTrip = cardClasses[cardClasses.length - 1]
//             if (spotTrip != cardTrip) {
//                 card.classList.add("hide")
//             }
//         })
//     }
// })


$(document).ready(function () {
    $(".pilihstreamerbtn").on("click", function () {
        $("#streamerhide").show();
        $("#streamerhide")[0].scrollIntoView();
    });
});

