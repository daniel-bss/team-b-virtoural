var swiper_tentang_kami = new Swiper(".swiper-tentang-kami", {
    slidesPerView: "auto",
    centeredSlides: true,
    // spaceBetween: 15,
    speed: 800,
    grabCursor: true,
    loop: true,
    navigation: {
        nextEl: ".button-next-tentang-kami",
        prevEl: ".button-prev-tentang-kami",
      },
      pagination: {
        el: ".pagination-tentang-kami",
        clickable: true,
      },
  });