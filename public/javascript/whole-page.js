// === Initiate ===
intro()

// === function list ===
function coba(){
    $("#nav").hide()
}
function intro(){
    // coba()
    // konfirmasiHide()
    // disetujuiHide()
    // ditinjauHide()
    // pembayaranBerhasilHide()
    // selesaiHide()
}
function introShow(){
    // coba()
    $(".bullet-progress-img-konfirmasi").show()
    $(".bullet-progress-streamer-belum-bisa").show()
    $(".masih-dalam-proses-container").show()
    $(".wrapper-button-pesan-tiket-batalkan").show()
    $(".streamer-belum-bisa-container").show()
    $("#perbarui-address").show()
}
function konfirmasiHide(){
    // introShow()
    // $(".bullet-progress-img-konfirmasi").hide()
    // $(".bullet-progress-streamer-belum-bisa").hide()
    $("#perbarui-address").hide()
    // $(".masih-dalam-proses-container").hide()
    // $(".streamer-belum-bisa-container").hide()
    $(".wrapper-button-pesan-tiket-batalkan").hide()
}
function konfirmasi(){
    // introShow()
    // $(".bullet-progress-img-konfirmasi").show()
    // $(".masih-dalam-proses-container").show()
    // $(".wrapper-button-pesan-tiket-batalkan").show()
    $(".bullet-progress-img-awal-satu").hide()
    $(".wrapper-button-pesan-tiket").hide()
    $("#submit-address").hide()
}
function backToIntro(){
    // intro()
    $(".bullet-progress-img-awal-satu").show()
    $(".wrapper-button-pesan-tiket").show()
}
function disetujuiHide(){
    $(".disetujui-container").hide()
    // $(".hidden-waktu-upload-pembayaran").hide()
    $(".rekening-tujuan-container").hide()
    // $(".bullet-progress-disetujui").hide()
    $(".wrapper-button-lihat-detail-pesanan").hide()
}
function disetujuiShow(){
    // konfirmasiHide()
    $(".disetujui-container").show()
    $(".rekening-tujuan-container").show()
    // $(".bullet-progress-disetujui").show()
    $(".wrapper-button-lihat-detail-pesanan").show()
    $(".bullet-progress-img-awal-satu").hide()
    $(".wrapper-button-pesan-tiket").hide()
    $(".waktu-order-content-container").hide()
    $(".button-batalkan-dua").hide()
    $(".bullet-progress-img-awal-dua").hide()
    $(".destinasi-service-tambahan-wrapper").hide()
}
function ditinjauHide(){
    $(".disetujui-container").hide()
    $(".hidden-waktu-upload-pembayaran").hide()
    $(".rekening-tujuan-container").hide()
    $(".peninjauan-gagal-container").hide()
    // $(".bullet-progress-disetujui").hide()
    $(".wrapper-button-lihat-detail-pesanan").hide()
    // $(".bullet-progress-ditinjau").hide()
    // $(".bullet-progress-peninjauan-gagal").hide()
    $(".masih-ditinjau-container").hide()
}
function ditinjauShow(){
    // konfirmasiHide()
    $(".masih-ditinjau-container").show()
    $(".peninjauan-gagal-container").show()
    // $(".bullet-progress-ditinjau").show()
    // $(".bullet-progress-peninjauan-gagal").show()
    $(".hidden-waktu-upload-pembayaran").show()
    $(".rekening-tujuan-container").show()
    $(".wrapper-button-lihat-detail-pesanan").show()
    $(".disetujui-container").hide()
    // $(".bullet-progress-disetujui").hide()
    $(".bullet-progress-img-awal-satu").hide()
    $(".wrapper-button-pesan-tiket").hide()
    $(".waktu-order-content-container").hide()
    $(".button-batalkan-dua").hide()
    $(".bullet-progress-img-awal-dua").hide()
    $(".destinasi-service-tambahan-wrapper").hide()
}
function pembayaranBerhasilHide(){
    $(".link-zoom-container").hide()
    $(".wrapper-button-trip-selesai").hide()
    $(".pembayaran-berhasil-container").hide()
    $(".bullet-progress-pembayaran-berhasil").hide()
}
function pembayaranBerhasilShow(){
    // konfirmasiHide()
    // disetujuiHide()
    // ditinjauHide()
    $(".link-zoom-container").show()
    $(".wrapper-button-trip-selesai").show()
    $(".pembayaran-berhasil-container").show()
    $(".bullet-progress-pembayaran-berhasil").show()
    $(".waktu-order-content-container").show()
    $(".destinasi-service-tambahan-wrapper").show()
    $("#perbarui-address").show()
    $(".bullet-progress-img-awal-tiga").hide()
    $("#submit-address").hide()
    $(".bullet-progress-img-awal-satu").hide()
    $(".bullet-progress-img-awal-dua").hide()
    $(".wrapper-button-pesan-tiket").hide()
}
function selesaiHide(){
    $(".bullet-progress-selesai-tunggu").hide()
}
function selesaiShow(){
    // konfirmasiHide()
    // disetujuiHide()
    // ditinjauHide()
    pembayaranBerhasilHide()
    $(".bullet-progress-selesai-tunggu").show()
    $(".bullet-progress-img-awal-empat").hide()
    $(".button-trip-selesai").hide()
}
// === On-Click ===

// ===Intro to Konfirmasi====
$(".button-pesan-tiket").on("click", () => {
    konfirmasi()
})
// ===Konfirmasi to Intro===
$(".button-batalkan-dua").on("click", () => {
    backToIntro()
})
// ===Konfirmasi to disetujui===
$(".bullet-progress-img-awal-dua").on("click", () => {
    disetujuiShow()
})
// ===Disetujui to ditinjau===
$("#submitbtn").on("click", () => {
    ditinjauShow()
})
$(".image-tiga-bullet").on("click", () => {
    pembayaranBerhasilShow()
})
$(".button-trip-selesai").on("click", () => {
    selesaiShow()
})
// ===Konfirmasi to Disetujui===


// $(".button-pesan-tiket").click(function(){
//     console.log("test")
//     $(".bullet-progress-img-konfirmasi").show()
// })