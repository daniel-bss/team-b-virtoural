var swiper_destinasi = new Swiper(".swiper-destinasi", {
  slidesPerView: "auto",
  spaceBetween: 30,
  slidesPerGroup: 4,
  speed:800,
  // loop: true,
  grabCursor: true,
  loopFillGroupWithBlank: false,
  pagination: {
    el: ".pagination-destinasi",
    clickable: true
  },
  navigation: {
    nextEl: ".button-next-destinasi",
    prevEl: ".button-prev-destinasi"
  },
});