var swiper_streamerTerbaik = new Swiper(".swiper-streamer-terbaik", {
  slidesPerView: "auto",
  spaceBetween: 30,
  slidesPerGroup: 4,
  speed:800,
  // loop: true,
  grabCursor: true,
  loopFillGroupWithBlank: false,
  pagination: {
    el: ".pagination-streamer-terbaik",
    clickable: true
  },
  navigation: {
    nextEl: ".button-next-streamer-terbaik",
    prevEl: ".button-prev-streamer-terbaik"
  },
});