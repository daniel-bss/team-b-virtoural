// $(function(){

//     // jQuery methods go here...

// });

// ================================================== NAV USER

// $(".masuknavuser").click(function(){
//     $("#nav-user").show()
//     $("#nav").hide()
//     $("#loginmodal").hide()
// })

$(".avatardropdownbtn").click(function(){
    $(".avatardropdown").toggle()
})

// $(".keluarbtn").click(function(){
//     $("#nav-user").hide()
//     $("#nav").show()
// })

// ================================================== POP UP MODAL LOGIN

// $("#loginbtn").click(function(){
//     $("#loginmodal").show()
// })

$(".cancelbtn").click(function(){
    $("#loginmodal").fadeOut()
    window.location.search = ""
})

$(window).click(function(event) {
    if (event.target.id == "loginmodal") {
        $("#loginmodal").fadeOut()
        window.location.search = ""
    }
})

// ================================================== POP UP MODAL DAFTAR

$(".daftarbtn").click(function(){
    $("#daftarmodal").fadeIn()
    console.log("yeahh")
})

$("#masukbtn").click(function(){
    $("#daftarmodal").hide()
    $("#loginmodal").fadeIn()
})

$(".cancelbtn").click(function(){
    $("#daftarmodal").fadeOut()
    window.location.search = ""
})

$(window).click(function(event) {
    if (event.target.id == "daftarmodal") {
        $("#daftarmodal").fadeOut()
        window.location.search = ""
    }
})

// ================================================== EXTEND DATA STREAMER

$('input[type="radio"]').click(function(){
    if($(this).attr("value")=="tidak"){
        $("#extenddatastreamer").hide();
    }
    if($(this).attr("value")=="iya"){
        $("#extenddatastreamer").show();

    }        
});
$('input[type="radio"]').trigger('click');

// ================================================== TOGGLE PASSWORD

$(".toggle-psw").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash")
    var i = $($(this).attr("toggle"))
    if (i.attr("type") == "password") {
        i.attr("type", "text")
    } else {
        i.attr("type", "password")
    }
})

// ================================================== POP UP MODAL LUPA PW

$("#lupabtn").click(function(){
    $("#lupamodal").show()
    $("#loginmodal").hide()
})

$(".masukemail").click(function(){
    $("#lupamodal2").show()
    $("#lupamodal").hide()
})

$(".buatpsw").click(function(){
    $("#lupamodal3").show()
    $("#lupamodal2").hide()
})

$(".buatpsw2").click(function(){
    $("#lupamodal4").show()
    $("#lupamodal3").hide()
})

$(".masukvt").click(function(){
    $("#lupamodal4").hide()
})

$(".cancelbtn").click(function(){
    $("#lupamodal").hide()
    $("#lupamodal2").hide()
    $("#lupamodal3").hide()
    $("#lupamodal4").hide()
})

$(window).click(function(event) {
    if (event.target.id == "lupamodal") {
        $("#lupamodal").hide()
    }
})

$(window).click(function(event) {
    if (event.target.id == "lupamodal2") {
        $("#lupamodal2").hide()
    }
})

$(window).click(function(event) {
    if (event.target.id == "lupamodal3") {
        $("#lupamodal3").hide()
    }
})

$(window).click(function(event) {
    if (event.target.id == "lupamodal4") {
        $("#lupamodal4").hide()
    }
})

$(".masukemail").click(function(){
    $("#lupamodal2").show()
    $("#lupamodal").hide()
})


const url = window.location.search.split("?")[1]
if (url === "login=true") {
    $("#loginmodal").fadeIn()
} else if (url === "register=true") {
    $("#daftarmodal").fadeIn()
}

$(window).scroll(function () {
    if (document.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        $("#myBtn").show()
    } else {
        $("#myBtn").hide()
    }
})

$("#myBtn").click(function () {
    $(window).scrollTop(0)
})