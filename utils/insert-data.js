const {Streamers, Destinations} = require('../models')

const streamers = [
    {
        name: "Putri",
        image: "putri.png"
    },
    {
        name: "Andrew",
        image: "andrew.png"
    },
    {
        name: "Kamila",
        image: "kamila.png"
    },
    {
        name: "Mutia",
        image: "mutia.png"
    },
    {
        name: "Putra",
        image: "putra.png"
    },
    {
        name: "Adela",
        image: "adela.png"
    },
    {
        name: "Dimas",
        image: "dimas.png"
    },
    {
        name: "Mirza",
        image: "mirza.png"
    },
    {
        name: "Putri",
        image: "putri.png"
    },
    {
        name: "Putri",
        image: "putri.png"
    },
    {
        name: "Putri",
        image: "putri.png"
    },
    {
        name: "Putri",
        image: "putri.png"
    }
]

const destinations = [
    {
        name: "Lombok",
        image: "lombok.png"
    },
    {
        name: "Bali",
        image: "bali.png"
    },
    {
        name: "Yogyakarta",
        image: "yogya.png"
    },
    {
        name: "Papua",
        image: "papua.png"
    },
    {
        name: "Medan",
        image: "medan.png"
    },
    {
        name: "Bandung",
        image: "bandung.png"
    },
    {
        name: "Bangka",
        image: "bangka.png"
    },
    {
        name: "Malang",
        image: "malang.png"
    },
    {
        name: "Yogyakarta",
        image: "yogya.png"
    },
    {
        name: "Yogyakarta",
        image: "yogya.png"
    },
    {
        name: "Yogyakarta",
        image: "yogya.png"
    },
    {
        name: "Yogyakarta",
        image: "yogya.png"
    },
]

async function InsertData(req, res) {
    for (let i = 0; i < streamers.length; i++) {
        Streamers.create({
            streamer_fullname: streamers[i].name,
            streamer_email: streamers[i].name + "@email.com",
            description: "deskripsi adalah ...",
            streamer_password: "abc123",
            is_streamer: true,
            birthday: "January 1st, 1970",
            streamer_address: "adress123",
            phone_number: "081234567",
            gender: ["Male", "Female"][Math.round(Math.random())],
            image_streamer: streamers[i].image,
            rating: Math.floor(Math.random() * 5) + 1,
        })
    }
    
    for (let i = 0; i < destinations.length; i++) {
        Destinations.create({
            destination_name: destinations[i].name,
            rating: Math.floor(Math.random() * 5) + 1,
            total_streamer: 10,
            total_spot: 5,
            image: destinations[i].image,
            description: "deskripsi destinasi",
            price: 123000,
            date: "Kamis, 1 Januari 1970",
            time: "Jam 00.00",
        })
    }
    res.redirect('/')
}

module.exports = InsertData