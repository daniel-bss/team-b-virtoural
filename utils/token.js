const jwt = require("jsonwebtoken")

class Token{
  static makeToken(payload){
    return jwt.sign(payload, "privatekey")
  }

  static decodeToken(token){
    console.log(token)
    try {
      const decoded = jwt.verify(token, "privatekey")
      if(!decoded){
        return null
      }
      return decoded

    } catch (error) {
      return null
    }
  }
}

module.exports = Token